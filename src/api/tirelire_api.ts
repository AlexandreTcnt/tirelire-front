const baseUrl = 'api/tirelire/'

export function listTirelires() {
  return fetch(baseUrl + 'list/').then((response) => {
    return response.json()
  })
}

export function addTirelire(search: string) {
  return fetch(baseUrl + 'init/', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ owner: search })
  }).then((response) => {
    return response.json()
  })
}

export function saveUp(owner: string, amount: number) {
  return fetch(baseUrl + 'save-up/', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ owner: owner, amount: amount })
  }).then((response) => {
    return response.json()
  })
}

export function spend(owner: string) {
  return fetch(baseUrl + 'spend/', {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ owner: owner })
  }).then()
}
