# Tirelire front

This is the front side of the application. It is developed in Vuejs3 (typescript).

## Getting started locally

To build the application locally and dev, you need to create a .env file with the following environment variable:

VUE_APP_BACKEND_URL=the web server address

Then run the following commands :

`npm install`

`npm run dev`

The app should be reachable on the specified webserver address.

## Run with docker

To run the application with docker:

`docker build -t tirelire_front .`

`docker run -p 8080:8080 -it tirelire_front`

The application should be available on port 8080.
To run the entire stack, please refer to the backend documentation.